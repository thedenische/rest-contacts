package com.example.rest.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "APPLICATION")
public class Application {
    @Id
    @GeneratedValue(generator = "SEQ_APPLICATION", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "SEQ_APPLICATION", sequenceName = "SEQ_APPLICATION", allocationSize = 1)
    @Column(name = "APPLICATION_ID")
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "DT_CREATED", nullable = false, updatable = false)
    private Date createdAt;

    @Column(name = "PRODUCT_NAME")
    private String productName;

    @ManyToOne
    @JoinColumn(name = "CONTACT_ID", nullable = false)
    private Contact contact;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    @PrePersist
    public void onPrePersist() {
        this.createdAt = new Date();
    }
}
