package com.example.rest.repository;

import com.example.rest.model.Application;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ApplicationRepository extends JpaRepository<Application, Long> {

    Optional<Application> findTopByContactIdOrderByCreatedAtDesc(long contactId);
}
