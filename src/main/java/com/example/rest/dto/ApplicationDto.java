package com.example.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class ApplicationDto {

    @JsonProperty("APPLICATION_ID")
    private Long id;

    @JsonProperty("DT_CREATED")
    private Date createdAt;

    @JsonProperty("PRODUCT_NAME")
    private String productName;

    @JsonProperty("CONTACT_ID")
    private Long contactId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Long getContactId() {
        return contactId;
    }

    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }
}
